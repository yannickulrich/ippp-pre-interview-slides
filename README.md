# IPPP pre-interview event

[These are slides](https://gitlab.com/yannickulrich/ippp-pre-interview-slides/-/jobs/artifacts/root/file/welcome.pdf?job=buildslides) for the pre-interview event at IPPP.

## Contribution guide
Contributors, please add your profile by filling out [the template](https://gitlab.com/yannickulrich/ippp-pre-interview-slides/-/blob/root/profiles/__template.tex)
 * create a copy at `profiles/<first name>.tex`
 * your name needs to be `<First Name>-<Last Name>`
 * add an image under `pics/<First Name>-<Last Name>.jpg` (ideally
   something not physics-related :wink:)
 * make sure that your year is just an integer

This uses git submodules, make sure to either
```shell
$ git clone --recursive git@gitlab.com:yannickulrich/ippp-pre-interview-slides
```
or once you've cloned
```shell
$ git submodule init && git submodule update
```
